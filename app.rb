$stdout.sync = true

require 'sinatra'
require 'sinatra/contrib/all'
require "http"
require "sinatra/reloader"
require 'yaml'
require "pp"
require 'ostruct'
require 'json'
require 'sinatra/cross_origin'

set :bind, '0.0.0.0'
set :port, ENV["port"] || 9999

configure do
  enable :cross_origin
end

$server = ENV["server"] || "http://api.geointranet"
# read conf and create table route
dir_conf = ENV["dir_conf"] || "conf"
ymlfiles = Dir["#{dir_conf}/*.yml"]
endpoints = {}
ymlfiles.each do |file| 
	newConfig = YAML.load_file(file)
	endpoints = endpoints.merge(newConfig){ |key,oldval,newval| oldval | newval }
end
rawEndpoints = endpoints["endpoints"]

MicroGateway = OpenStruct.new
MicroGateway.endpoint = []
MicroGateway.endpoint_list = []
rawEndpoints.each do |endpoint|
	endp = OpenStruct.new
	edata = endpoint["endpoint"].split(":")
	endp.http_method = edata[0]
	endp.uri = edata[1]
    puts "endpoint loaded: #{endp.uri}"
	endp.backend = []
	endpoint["backend"].each do |bke|
		backend_object = OpenStruct.new
		bdata = bke.split(":")
		backend_object.http_method = bdata[0]
		backend_object.uri = bdata[1]
		backend_object.filter = bdata[2].nil? ? "return" : bdata[2]
		endp.backend  << backend_object
	end
	MicroGateway.endpoint_list << endp.uri
	MicroGateway.endpoint << endp
end

def loggerFormat(loggerInstance)
	puts "======================="
	puts JSON.pretty_generate(loggerInstance.to_h)
	puts "======================="
end

def requests_chain(backend_list,params,chainMetadata)
	loggableChain = OpenStruct.new
	params_chain = params
	response = {}
	params_prepared = params
	chain_id = SecureRandom.hex
	loggableChain.id = chain_id
	loggableChain.date = DateTime.now
	loggableChain.uri = chainMetadata[:uri]
	loggableChain.httpMethod = chainMetadata[:httpMethod]
	loggableChain.chain = []
#	d "CHAIN============================================="
	backend_list.each do |backend|
		smethod = backend.http_method.downcase
		suri = backend.uri.downcase
		loggableLink = OpenStruct.new
		loggableLink.uri = suri
		loggableLink.link_id = SecureRandom.hex
		loggableLink.link_date = DateTime.now
		if backend.filter == "original_request"
			params_prepared = params
		end
		
		send_data = {:json => params_prepared}
		response = HTTP.send(smethod,$server+suri, send_data )
		params_prepared_filtered = params_prepared.clone
		params_prepared_filtered["password"] = "****************"
		acceptable_codes = [200,204,201]
		code = response.status.to_s.gsub(/\D/, '').to_i
		loggableLink.httpCode = code
		loggableLink.httpMethod = smethod
		loggableLink.filter = backend.filter
		loggableLink.status = response.status
		loggableLink.requestParams = params_prepared_filtered.to_s
		loggableLink.response = response.to_s
		loggableChain.chain << loggableLink.to_h

		if !(acceptable_codes.include? code)
			loggerFormat loggableChain
			return {response: response, requestId: loggableChain.id , error: true }
		end
		responseReq = JSON.parse(response.to_s,object_class: Hash) rescue {}
		if backend.filter == "noreturn"
			params_prepared = responseReq
        elsif backend.filter == "bypass"
			# just bypass
		else
			params_prepared_filtered = params_prepared_filtered.merge(responseReq)
			params_prepared = params_prepared.merge(responseReq)
		end
		params_prepared_filtered["password"] = "****************"
		loggableChain.requestParams = params_prepared_filtered.to_s
	end
	loggerFormat loggableChain
	return  {response: response, error: false}
end


####
route :get, :post, :delete, :put, :options, '/*' do
  # "GET" or "POST"
  if request.env["REQUEST_METHOD"].to_s.upcase == "OPTIONS"
  	status 200
  	return ""
  end
  send_params = params
  if request.content_type == "application/json"
  	bodyJson = JSON.parse(request.body.read)
  	send_params = bodyJson
  end
  uri = request.path_info
  if uri[-1] == "/"
  	uri = uri[0..-2]
  end
  tError = true
  tEndpoint = nil
  indexEndpoint = MicroGateway.endpoint_list.index(uri)
  if !indexEndpoint.nil?
  	tError = false
  	tEndpoint = MicroGateway.endpoint[indexEndpoint]
 # 	p "found! "
  end

  if !tEndpoint.nil?
  	if tEndpoint.http_method != request.env["REQUEST_METHOD"]
  		tError = true
  	end
  end

  if tError
  	status 400
  	return "ERROR -  Meditation Machine - BAD Req"
  else
  	chainMetadata = {
  		 uri: uri,
  		 httpMethod: tEndpoint.http_method
  	}
  	responseChain = requests_chain(tEndpoint.backend,send_params,chainMetadata)
  	if responseChain[:error]
  		status responseChain[:response].status
  		return "{status: 'fail', requestId: #{responseChain[:requestId]} }"
  	end
  	status responseChain[:response].code
  	return responseChain[:response].to_s
  end
end